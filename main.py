class Image:
    def display(self):
        pass
class RealImage(Image):
    def __init__(self,filename):
        self.filename=filename
        self.load_image_from_disk()
    def load_image_from_disk(self):
        print(f"Loading image from disk: {self.filename}")
    def display(self):
        print(f"Displaying image: {self.filename}")
class ProxyImage(Image):
    def __init__(self,filename):
        self.real_image=None
        self.filename=filename
    def display(self):
        if self.real_image is None:
            self.real_image=RealImage(self.filename)
        self.real_image.display()

# Client code
image1 = ProxyImage("image1.jpg")
image2 = ProxyImage("image2.jpg")

image1.display()  # This will load and display "image1.jpg"
image2.display()  # This will load and display "image2.jpg"
image1.display() # This won't load it will only display